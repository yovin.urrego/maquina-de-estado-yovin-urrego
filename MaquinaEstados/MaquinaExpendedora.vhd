----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:40:17 05/22/2022 
-- Design Name: 
-- Module Name:    MaquinaExpendedora - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MaquinaExpendedora is
    Port ( 	clk : in  STD_LOGIC;
				ingresoM: inout  STD_LOGIC;
				verificado,  soltar : out  STD_LOGIC;
				selecion, enviar : inout  STD_LOGIC_VECTOR (3 downto 0);
				moneda : in  STD_LOGIC_VECTOR (1 downto 0);
				aux : inout  STD_LOGIC_VECTOR (2 downto 0));
end MaquinaExpendedora;

architecture Behavioral of MaquinaExpendedora is
Type estado is (A, B, C, D, E);
signal estado_fut, estate: estado;
begin
process (ingresoM, estate, moneda) begin
case estate is 
--Estado A
when A => 
	aux <= "000";
	soltar <= '0';
	if ingresoM = '1' then estado_fut <= B;
	else estado_fut <= A;
	end if;
--Estado B	
when B =>
	aux <= "001";
	if	moneda = "11" then verificado <= '1';
	estado_fut <= C;
	else verificado <= '0';
	estado_fut <= A;
	end if;
--Estado C
when C =>
	aux <= "010";
	if selecion = "UUUU" then estado_fut <= C;
	else estado_fut <= D;
	end if;
--Estado D	
when D => 
	aux <= "011";
	enviar <= selecion;
	estado_fut <= E;
--Estado E	
when E =>
	aux <= "100";
	soltar <= '1';
	ingresoM <= '0';
	verificado <= '0';
	selecion <= "UUUU";
	estado_fut <= A;
	end case;
end process; 

process (clk) begin
if (clk' event and clk ='1') then
estate <= estado_fut;
end if;
end process;
end Behavioral;

